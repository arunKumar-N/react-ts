import * as ReactDOM from "react-dom/client";

import React from "react";
import Root from "./src/root";

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
ReactDOM.createRoot(document.getElementById("root")!).render(
	<React.StrictMode>
		<Root/>
	</React.StrictMode>
);