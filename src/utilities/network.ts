import { getAllEndpoints, getBaseURL } from "Config/endpoints";

import authDetails from "Config/authHeaders";
import axios from "axios";
import cookie from "react-cookies";
import { deepClone } from "Utilities/helpers";

const headers = {
	Accept: "application/json",
	"Content-Type": "application/json",
	"Access-Control-Allow-Origin": "*",
};


function Network(this: any) {
	this.endpoints = getAllEndpoints();
	this.baseURL = getBaseURL();
	this.url = "";
	this.transformRequest = (data = {}) => {
		return Object.entries(data)
			.map((x:any) => `${encodeURIComponent(x[0])}=${encodeURIComponent(x[1])}`)
			.join("&");
	};
	this.setBaseURL = () => {
		this.instance.defaults.headers = {
			...this.instance.defaults.headers,
			...authDetails,
		};
	};
	this.instance = axios.create({
		baseURL: this.baseURL,
		timeout: 60000,
		withCredentials: false,
		headers: headers,
	});
  
	this.instance.interceptors.request.use(
		function (config: any) {
			return config;
		},
		function (error: any) {
			return Promise.reject(error);
		}
	);
  
	this.instance.interceptors.response.use(
		function (response: any) {
			return response;
		},
		function (error: { response: { status: number; }; }) {
			if (error?.response?.status == 401) {
				cookie.remove("access_token");
				cookie.remove("id_token");
				(window as any).location = "/";
			}
			return Promise.reject(error);
		}
	);
  
	return this;
}


Network.prototype.convertToJsonString = function (request: any) {
	return { jsonString: JSON.stringify(request) };
};
  
Network.prototype.api = function (url: string | number) {
	this.setBaseURL();
	if (url) {
		this.url = this.endpoints[url];
	} else {
		this.url = "";
	}
  
	return this;
};
  
const replacePathParams = (url: string, params: any[]) => {
	params.forEach((p: any, index: number) => {
		if (url.indexOf("path" + (index + 1)) > -1) {
			url = url.replace("path" + (index + 1), p);
		} else {
			url += `/${p}`;
		}
	});
	return url;
};
  
const replaceQueryParams = (url: string, params: { [x: string]: string; }) => {
	if (url && params) {
		const urlParamsArray = Object.keys(params);
		urlParamsArray.forEach((q) => {
			if (url.indexOf(q) > -1) {
				url = url.replace(q + "=", q + "=" + params[q]);
			}
		});
	}
	return url;
};
  
Network.prototype.apiWithPath = function (url: string | number, params: any) {
	this.setBaseURL();
	if (url) {
		this.url = this.endpoints[url];
		this.url = replacePathParams(this.url, params);
	} else {
		this.url = "";
	}
  
	return this;
};
  
/**
   * This method add the path parameter value in url.
   * Name should be same of path parameter as mentiond in endpoint.js
   */
Network.prototype.apiWithQuery = function (url: string | number, params: any) {
	this.setBaseURL();
	if (url) {
		this.url = this.endpoints[url];
		this.url = replaceQueryParams(this.url, params);
	} else {
		this.url = "";
	}
	return this;
};
  
Network.prototype.apiWithPathAndQuery = function (
	url: string | number,
	pathParams: any,
	queryParams: any
) {
	this.setBaseURL();
	if (url) {
		this.url = this.endpoints[url];
		this.url = replacePathParams(this.url, pathParams);
		this.url = replaceQueryParams(this.url, queryParams);
	} else {
		this.url = "";
	}
	return this;
};
  
Network.prototype.get = function (params: any) {
	const instance = deepClone(this.instance);
	return instance.get(
		this.url,
		{ params },
		{
			headers: { ...headers, ...instance.defaults.headers },
		}
	);
};
  
Network.prototype.post = function (data: any, headers: any) {
	const instance = deepClone(this.instance);
  
	return instance.post(this.url, data, {
		headers: { ...headers, ...instance.defaults.headers },
	});
};
Network.prototype.put = function (data: any, headers: any) {
	const instance = deepClone(this.instance);
	return instance.put(this.url, data, {
		headers: { ...headers, ...instance.defaults.headers },
	});
};
Network.prototype.delete = function () {
	const instance = deepClone(this.instance);
	return instance.delete(this.url);
};
  
export default Network;
