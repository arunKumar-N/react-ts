import { IContext } from "Utilities/interface";
import React from "react";
import RefContext from "Utilities/refContext";

const RefProvider = (props: { data: IContext; children: JSX.Element; }): JSX.Element => {
	const { data, children } = props;
	return <RefContext.Provider value={data}>{children}</RefContext.Provider>;
};

export default RefProvider;
