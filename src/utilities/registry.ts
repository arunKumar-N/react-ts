// Controllers
import Home from "Pages/home";
import Second from "Pages/second";

function Injector(component:any, name:string) {
	const hoc = component;
	hoc.displayName = name;
	return hoc;
}

export default {
	Home: Injector(Home, "Home"),
	Second: Injector(Second, "Second"),
};
