import { IContext } from "Utilities/interface";
import React from "react";

const RefContext = React.createContext<IContext | null>(null);

RefContext.displayName = "RefContext";

export default RefContext;
