import { History, Location } from "history";

import { match } from "react-router";

// types
type TPayload = string | null | number | object;

type TEndpointObject = {
	[key: string]: string;
}

type TDynamicObject = {
	[keyname: string]: string  | object | Array<any>;
}
// interface
interface IStoreData {
	[storeName: string]: string | null | undefined  | object | Array<any>;
}

interface IActions {
	[actionType: string]: (...args: any) => void;
}


interface IContext {
    store: IStoreData,
    actions: IActions;
	history:History;
	location:Location;
}

interface IContextProps extends IContext {
    match:match;
}

export {
	IContext,
	IContextProps,
	TPayload,
	TEndpointObject,
	TDynamicObject,
	IStoreData
};