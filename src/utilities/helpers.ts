import { IContextProps } from "Utilities/interface";
const setNamespace = (namespace:string) => (constant:string) => {
	return `${namespace}/${constant}`;
};

type DataObject = {
	[key: string]: any;
}
const deepClone = (data:DataObject) => {
	return JSON.parse(JSON.stringify(data));
};

const formStoreData = (props:IContextProps, namespaces:string[] = []) => {
	let store = {};
	let actions = {};
  
	namespaces.forEach((namespace:string) => {
		store = { ...store, ...(props.store[`${namespace}Store`] as object) };
		actions = { ...actions, ...props.actions[`${namespace}Actions`] };
	});
	return {
		store,
		actions,
		history: props.history,
		location: props.location,
	};
};

  
export {
	setNamespace,
	deepClone,
	formStoreData,
};
