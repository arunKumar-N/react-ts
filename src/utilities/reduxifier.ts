/* eslint-disable prefer-const */
import { History, createBrowserHistory } from "history";
import { applyMiddleware, bindActionCreators, combineReducers, compose, createStore } from "redux";
import { connectRouter, routerMiddleware } from "connected-react-router";

import { Dispatch } from "redux";
import HomeDucks from "Ducks/home.ducks";
import { IStoreData } from "Utilities/interface";
import { connect } from "react-redux";
import { deepClone } from "Utilities/helpers";
import promiseMiddleware from "redux-promise";
import thunk from "redux-thunk";

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const history = createBrowserHistory();

interface IDucks {
	[key: string]:  {
		namespace: string,
		store: object,
		reducer: (state:any, action:any) => void,
		creators: object,
	}
}
// Generates root store and reducer
function generateBuildingBlocks() {
	let rootReducer = {};
	let rootStore:any = {};
	let creators:any = {};
  
	const ducks:IDucks = {
		HomeDucks,
	};
	const namespaces:string[] = [];
  
	Object.keys(ducks).forEach(function iterateDucksBB(duck) {
		if (Object.prototype.hasOwnProperty.call(ducks, duck)) {
			const splitName:string= ducks[duck].namespace.split("/")[0];
			namespaces.push(splitName);
			rootReducer = Object.assign(rootReducer, {
				[splitName]: ducks[duck].reducer,
			});
			rootStore = Object.assign(rootStore, { [splitName]: ducks[duck].store });
			creators = Object.assign(creators, { [splitName]: ducks[duck].creators });
		}
	});
  
	// workflow will be the root store tree
	rootStore = deepClone(rootStore);
  
	return { rootStore, rootReducer, creators, namespaces };
}

// Add a router reducer and combine remaining reducers into one.
function fuseReducers(reducers:any, historyObj:History) {
	const combinedReducer = {
		router: connectRouter(historyObj),
		...reducers,
	};
  
	return combineReducers(combinedReducer);
}


// Create a store tree
function createStoreTree(store:any, reducer:any, historyObj:History) {
	return createStore(
		reducer,
		store,
		composeEnhancers(
			applyMiddleware(
				routerMiddleware(historyObj),
				promiseMiddleware,
				thunk
			)
		)
	);
}

// Bind react containers with redux
function bindReactRedux(containers:any) {
	let bindings = {};
  
	// create required state and action objects
	function loopOver(ns:Array<string>, condition:string, input:any) {
		if (condition === "state") {
			return ns.reduce(
				function (acc:any, item:string) {
					acc.store[`${item}Store`] = input[item];
					return acc;
				},
				{ store: {} }
			);
		}
  
		// actions
		return ns.reduce(
			function (acc:any, item:string) {
				acc.actions[`${item}Actions`] = bindActionCreators(
					creators[item],
					input
				);
				return acc;
			},
			{ actions: {} }
		);
	}
  
	Object.keys(containers).forEach(function (entry) {
		if (Object.prototype.hasOwnProperty.call(containers, entry)) {
			const mapStateToProps = (state:IStoreData) => {
				return loopOver(namespaces, "state", state);
			};
  
			const mapDispatchToProps = (dispatch:Dispatch) => {
				return loopOver(namespaces, "actions", dispatch);
			};
  
			bindings = Object.assign(bindings, {
				[entry]: connect(
					mapStateToProps,
					mapDispatchToProps
				)(containers[entry]),
			});
		}
	});
	return bindings;
}


let {
	rootReducer = {},
	rootStore = {},
	creators,
	namespaces,
} = generateBuildingBlocks();

rootReducer = fuseReducers(rootReducer, history);
rootStore = createStoreTree(rootStore, rootReducer, history);

export default {
	rootStore,
	history,
	bindReactRedux,
};
  