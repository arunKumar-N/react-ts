import { deepClone, setNamespace } from "Utilities/helpers";

import { Dispatch } from "redux";
import { TPayload } from "Utilities/interface";
import ns from "Utilities/network";

const namespace = "home";
const createAction = setNamespace(namespace);

// STORE
const initialState = {
	pageName: "Home",
};
  
const ASSIGN_TO_HOME_STORE = createAction("ASSIGN_TO_HOME_STORE");
const RESET_HOME_STORE = createAction("RESET_HOME_STORE");

const assignToAuthStore = (type:string, payload:TPayload) => ({
	type: ASSIGN_TO_HOME_STORE,
	meta: {
		type,
		payload,
	},
});
  
const resetAuthStore = () => (dispatch:Dispatch) => {
	dispatch({
		type: RESET_HOME_STORE,
		meta: {
			payload: null,
		},
	});
};

// Methods
const test = () => (dispatch:Dispatch) => {
	dispatch(assignToAuthStore("pageName", "Home is home"));
};

// Reducers
const homeReducer = (state = initialState, action:{type: string, meta: { type: string, payload:TPayload}}) => {
	const localState = deepClone(state);
  
	switch (action.type) {
	case ASSIGN_TO_HOME_STORE:
		localState[action.meta.type] = action.meta.payload;
		return { ...localState };
	case RESET_HOME_STORE:
		return initialState;
	default:
		return localState;
	}
};
  
export default {
	namespace,
	store: initialState,
	reducer: homeReducer,
	creators: {
		assignToAuthStore,
		resetAuthStore,
		test,
	},
};
  