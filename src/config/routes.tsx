import { Route, Switch, withRouter } from "react-router-dom";

import React from "react";

const fetchRoutes = (containers:any) => {
	const { Home, Second } = containers; 

	// react-router
	return function Routes() {
		return (
			<Switch>
				<Route exact path='/' component={withRouter(Home)} />
				<Route exact path='/second' component={withRouter(Second)} />
				{/* Router No Match - 404 */}
				<Route path="*">
                    This is not found
				</Route>
			</Switch>
		);
	};
};

export { fetchRoutes };
