import { TDynamicObject, TEndpointObject } from "Utilities/interface";

const development:TEndpointObject = {
	key: "value"
};

export function getAllEndpoints() {
	return Object.keys(development).reduce(function (acc:TDynamicObject, item:string) {
		acc[item] = development[item];
		return acc;
	}, {});
}

export function getBaseURL() {
	return "baseurl" + "/api";
}
