import HomeComponent from "./home";
import { IContextProps } from "Utilities/interface";
import React from "react";
import RefProvider from "Utilities/refProvider";
import { formStoreData } from "Utilities/helpers";

const Home = (props:IContextProps): JSX.Element => {
	const propShapes = formStoreData(props, ["home"]);
	return (
		<RefProvider data={propShapes}>
			<HomeComponent />
		</RefProvider>
	);
};

export default Home;