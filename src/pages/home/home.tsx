import React, { useContext } from "react";

import { IContext } from "Utilities/interface";
import RefContext from "Utilities/refContext";

const HomeComponent = (): JSX.Element => {
	const ctx:IContext | any = useContext(RefContext);
	const { actions, store } = ctx;
	const { test } = actions;

	return (
		<div onClick={() => test()}>
            This is {store?.pageName} component
		</div>
	);
};

export default HomeComponent;