import React, { useContext } from "react";

import RefContext from "Utilities/refContext";

const SecondComponent = (): JSX.Element => {
	const ctx = useContext(RefContext);
	console.log(ctx);
	return (
		<div>
            This is Second component
		</div>
	);
};

export default SecondComponent;