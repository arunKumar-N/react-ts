import { IContextProps } from "Utilities/interface";
import React from "react";
import RefProvider from "Utilities/refProvider";
import SecondComponent from "./second";
import { formStoreData } from "Utilities/helpers";

const Second = (props:IContextProps): JSX.Element => {
	const propShapes = formStoreData(props, ["home"]);

	return (
		<RefProvider data={propShapes}>
			<SecondComponent />
		</RefProvider>
	);
};

export default Second;