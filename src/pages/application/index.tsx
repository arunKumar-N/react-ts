import { Route, RouteComponentProps, Router } from "react-router-dom";

import React from "react";
import Reduxifier from "Utilities/reduxifier";
import Registry from "Utilities/registry";
import { fetchRoutes } from "Config/routes";

const Containers:any = Reduxifier.bindReactRedux(Registry);
const Routes = fetchRoutes(Containers);
const Application = (): JSX.Element => {
	const renderContent = (data:RouteComponentProps) => {
		console.log(data);
		return (
			<div className="content-container">
				<Routes />
			</div>
		);
	};

	return (
		<Router history={Reduxifier.history}>
			<Route render={(data:RouteComponentProps) => renderContent(data)} />
		</Router>
	);
 
};

export default Application;