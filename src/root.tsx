import "./root.css";

import Application from "Pages/application";
import { Provider } from "react-redux";
import React from "react";
import Reduxifier from "Utilities/reduxifier";

const Root = (): JSX.Element => {
	return (
		<Provider store={Reduxifier.rootStore}>
			<Application />
		</Provider>
	);
};

export default Root;