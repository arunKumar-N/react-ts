/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable no-undef */
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
	mode: "development",
	entry: "./index.tsx",
	devtool: "inline-source-map",
	devServer: {
		// inline: true,
		port: 8001,
		historyApiFallback: true,
	},  
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: "ts-loader",
				exclude: /node_modules/,
			},
			{
				test: /\.(css|scss)$/i,
				use: ["style-loader", "css-loader"],
			},      
		],
	},
	resolve: {
		modules: ["node_modules"],
		alias: {
			Pages: path.resolve(__dirname, "./src/pages"),
			Config: path.resolve(__dirname, "./src/config"),
			Ducks: path.resolve(__dirname, "./src/ducks"),
			Utilities: path.resolve(__dirname, "./src/utilities"),
		},
		extensions: [".tsx", ".ts", ".js"],
	},
	output: {
		filename: "bundle.js",
		path: path.resolve(__dirname, "dist"),
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: "./index.html",
		})
	]
};